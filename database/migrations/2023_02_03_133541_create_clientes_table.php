<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->id();
            $table->string('nome')->nullable(false);
            $table->string('cpf')->nullable(false)->unique(true);
            $table->string('rg')->nullable(false)->unique(true);
            $table->string('telefone')->nullable(false);
            $table->string('email')->nullable(true)->unique(true);
            $table->string('sexo')->nullable(true);
            $table->boolean('status')->nullable(false)->default(true);
            $table->dateTime('dataCadastro')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
};
