<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendas', function (Blueprint $table) {
            $table->id();
            $table->string('codigoVenda')->nullable(false);
            $table->decimal('valorTotal')->nullable(false);
            $table->dateTime('dataVenda')->nullable(false);
            $table->date('dataPagamento');
            $table->string('statusDaVenda')->nullable(false);
            $table->unsignedBigInteger('clienteId')->nullable(false);
            $table->foreign('clienteId')->references('id')->on('clientes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendas');
    }
};
