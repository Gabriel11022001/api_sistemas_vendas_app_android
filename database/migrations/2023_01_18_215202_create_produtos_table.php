<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('produtos', function (Blueprint $table) {
            $table->id();
            $table->string('nome')->nullable(false);
            $table->longText('descricao')->nullable(false);
            $table->decimal('precoCompra')->nullable(false);
            $table->decimal('precoVenda')->nullable(false);
            $table->boolean('status')->nullable(false)->default(true);
            $table->dateTime('dataCadastro')->nullable(false);
            $table->longText('urlImagemProduto')->nullable();
            $table->unsignedBigInteger('unidadeMedidaId')->nullable(false);
            $table->unsignedBigInteger('categoriaId')->nullable(false);
            $table->foreign('categoriaId')->references('id')->on('categorias');
            $table->foreign('unidadeMedidaId')->references('id')->on('unidade_medidas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('produtos');
    }
};
