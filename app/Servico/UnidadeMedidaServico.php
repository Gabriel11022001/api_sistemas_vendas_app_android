<?php

namespace App\Servico;

use App\Dtos\UnidadeMedidaDTO;
use App\Repositorio\UnidadeMedidaRepositorio;
use Exception;
use Illuminate\Http\Request;

class UnidadeMedidaServico
{

    public function cadastrarUnidadeMedida(Request $req) {
        $req->validate([
            'descricao' => 'required|unique:unidade_medidas',
            'status' => 'required|boolean'
        ],
        [   
            'descricao.required' => 'Informe a descrição da unidade de medida!',
            'descricao.unique' => 'Já existe uma unidade de medida cadastrada com essa descrição, '
            . ' informe outra descrição!',
            'status.required' => 'Informe o status da unidade de medida!',
            'status.boolean' => 'O status da unidade de medida deve ser um dado booleano!'
        ]);

        try {
            $unidadeMedidaDTO = new UnidadeMedidaDTO();
            $unidadeMedidaDTO->descricao = $req->descricao;
            $unidadeMedidaDTO->status = $req->status;
            $resultadoCadastrarUnidadeMedida = UnidadeMedidaRepositorio::cadastrarUnidadeMedida($unidadeMedidaDTO);

            if (is_null($resultadoCadastrarUnidadeMedida)) {
                
                return response()->json([
                    'msg' => 'Ocorreu um erro ao tentar-se cadastrar a unidade de medida!',
                    'dados' => null
                ], 500);
            }

            return response()->json([
                'msg' => 'Unidade de medida cadastrada com sucesso!',
                'dados' => $resultadoCadastrarUnidadeMedida
            ], 201);
        } catch (Exception $e) {

            return response()->json([
                'msg' => 'Ocorreu o seguinte erro: ' . $e->getMessage(),
                'dados' => null
            ], 500);
        }

    }

    public function buscarTodasUnidadesMedida() {

        try {
            $unidadesMedida = UnidadeMedidaRepositorio::buscarTodasUnidadesMedida();

            if (count($unidadesMedida) === 0) {

                return response()->json([
                    'msg' => 'Não existem unidades de medida cadastradas no banco de dados!',
                    'dados' => null
                ], 200);
            }

            $unidadesMedidaDTO = [];

            foreach ($unidadesMedida as $unidadeMedidaArray) {
                $unidadeMedidaDTO = new UnidadeMedidaDTO();
                $unidadeMedidaDTO->id = $unidadeMedidaArray['id'];
                $unidadeMedidaDTO->descricao = $unidadeMedidaArray['descricao'];
                $unidadeMedidaDTO->status = $unidadeMedidaArray['status'] === 1 ? true : false;
                $unidadesMedidaDTO[] = $unidadeMedidaDTO;
            }

            return response()->json([
                'msg' => 'Unidades de medida encontradas com sucesso!',
                'dados' => $unidadesMedidaDTO
            ], 200);
        } catch (Exception $e) {

            return response()->json([
                'msg' => 'Ocorreu o seguinte erro: ' . $e->getMessage(),
                'dados' => null
            ], 500);
        }

    }

    public function buscarUnidadeMedidaPeloId($id) {

        try {
            
            if (empty($id)) {
                throw new Exception('Informe o id da unidade de medida!');
            }

            $id = intval($id);
            $unidadeMedida = UnidadeMedidaRepositorio::buscarUnidadeMedidaPeloId($id);

            if (is_null($unidadeMedida)) {

                return response()->json([
                    'msg' => 'Unidade de medida não encontrada!',
                    'dados' => null
                ], 404);
            }

            $unidadeMedidaDTO = new UnidadeMedidaDTO();
            $unidadeMedidaDTO->id = $unidadeMedida->id;
            $unidadeMedidaDTO->descricao = $unidadeMedida->descricao;
            $unidadeMedidaDTO->status = $unidadeMedida->status === 1 ? true : false;

            return response()->json([
                'msg' => 'Unidade de medida encontrada!',
                'dados' => $unidadeMedidaDTO
            ], 200);
        } catch (Exception $e) {

            return response()->json([
                'msg' => 'Ocorreu o seguinte erro: ' . $e->getMessage(),
                'dados' => null
            ], 500);
        }

    }

    public function alterarStatusUnidadeMedida(Request $req) {
        $req->validate([
            'id' => 'required|numeric',
            'status' => 'required|boolean'
        ],
        [   
            'id.required' => 'Informe o id da unidade de medida!',
            'id.numeric' => 'O id da unidade de medida deve ser um valor numérico!',
            'status.required' => 'Informe o status da unidade de medida!',
            'status.boolean' => 'O status da unidade de medida deve ser um valor booleano!'
        ]);

        try {
            $id = intval($req->id);
            $status = $req->status;
            $resultadoEditarStatus = UnidadeMedidaRepositorio::alterarStatusUnidadeMedida($id, $status);

            if (is_null($resultadoEditarStatus)) {

                return response()->json([
                    'msg' => 'Ocorreu um erro ao tentar-se editar o status da unidade de medida!',
                    'dados' => null
                ], 500);
            }

            return response()->json([
                'msg' => 'O status da unidade de medida foi alterado com sucesso!',
                'dados' => $resultadoEditarStatus
            ], 200);
        } catch (Exception $e) {

            return response()->json([
                'msg' => 'Ocorreu o seguinte erro: ' . $e->getMessage(),
                'dados' => null
            ], 500);
        }

    }

    public function editarUnidadeMedida(Request $req) {
        $req->validate([
            'id' => 'required|numeric',
            'descricao' => 'required',
            'status' => 'required|boolean'
        ],
        [   
            'id.required' => 'Informe o id da unidade de medida!',
            'id.numeric' => 'O id deve ser um valor numérico!',
            'descricao.required' => 'Informe a descrição da unidade de medida!',
            'status.required' => 'Informe o status da unidade de medida!',
            'status.boolean' => 'O status da unidade de medida deve ser um dado booleano!'
        ]);

        try {
            $unidadeMedidaComDescricaoInformada = UnidadeMedidaRepositorio::buscarUnidadeMedidaPelaDescricao($req->descricao);

            if (count($unidadeMedidaComDescricaoInformada) > 0) {
                
                if ($unidadeMedidaComDescricaoInformada[0]['id'] != $req->id) {
                    
                    return response()->json([
                        'msg' => 'Já existe uma unidade de medida cadastrada com essa descrição, informe outra descrição!',
                        'dados' => null
                    ], 200);
                }

            }

            $unidadeMedidaDTO = new UnidadeMedidaDTO();
            $unidadeMedidaDTO->id = $req->id;
            $unidadeMedidaDTO->descricao = $req->descricao;
            $unidadeMedidaDTO->status = $req->status;
            $resultadoEditarUnidadeMedida = UnidadeMedidaRepositorio::editarUnidadeMedida($unidadeMedidaDTO);

            if (is_null($resultadoEditarUnidadeMedida)) {

                return response()->json([
                    'msg' => 'Ocorreu um erro ao tentar-se editar a unidade de medida!',
                    'dados' => null
                ], 500);
            }

            return response()->json([
                'msg' => 'Unidade de medida editada com sucesso!',
                'dados' => $resultadoEditarUnidadeMedida
            ], 200);
        } catch (Exception $e) {

            return response()->json([
                'msg' => 'Ocorreu o seguinte erro: ' . $e->getMessage(),
                'dados' => null
            ], 500);
        }

    }
}