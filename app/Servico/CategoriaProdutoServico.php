<?php

namespace App\Servico;

use App\Dtos\CategoriaProdutoDTO;
use App\Repositorio\CategoriaProdutoRepositorio;
use App\Repositorio\ProdutoRepositorio;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CategoriaProdutoServico
{

    public function cadastrarCategoriaProduto(Request $req) {
        $req->validate(
            [
                'descricao' => 'required',
                'status' => 'required|boolean'
            ],
            [   
                'descricao.required' => 'Informe a descrição da categoria!',
                'status.boolean' => 'O status da categoria deve ser um dado booleano!',
                'status.required' => 'Informe o status da categoria!'
            ]
        );

        try {
            $categoriaProdutoDTO = new CategoriaProdutoDTO();
            $categoriaProdutoDTO->descricao = $req->descricao;
            $categoriaProdutoDTO->status = $req->status;
            $res = CategoriaProdutoRepositorio::cadastrarCategoriaProduto($categoriaProdutoDTO);

            if (is_null($res)) {

                return response()->json([
                    'msg' => 'Ocorreu um erro ao tentar-se cadastrar a categoria!',
                    'dados' => null
                ], 500);
            }

            return response()->json([
                'msg' => 'Categoria cadastrada com sucesso!',
                'dados' => $res
            ], 201);
        } catch (Exception $e) {

            return response()->json([
                'msg' => 'Ocorreu o seguinte erro: ' . $e->getMessage(),
                'dados' => null
            ], 500);
        }

    }

    public function buscarTodasCategoriasProduto() {

        try {
            $categorias = CategoriaProdutoRepositorio::buscarTodasCategoriasProduto();

            if (count($categorias) === 0) {

                return response()->json([
                    'msg' => 'Não existem categorias cadastradas no banco de dados!',
                    'dados' => null
                ], 200);
            }

            $categoriasProdutoDTO = [];

            foreach ($categorias as $categoriaArray) {
                $categoriaDTO = new CategoriaProdutoDTO();
                $categoriaDTO->id = $categoriaArray['id'];
                $categoriaDTO->descricao = $categoriaArray['descricao'];
                $categoriaDTO->status = $categoriaArray['status'] === 1 ? true : false;
                $categoriasProdutoDTO[] = $categoriaDTO;
            }

            return response()->json([
                'msg' => 'Existem categorias cadastradas no banco de dados!',
                'dados' => $categoriasProdutoDTO
            ], 200);
        } catch (Exception $e) {

            return response()->json([
                'msg' => 'Ocorreu o seguinte erro: ' . $e->getMessage(),
                'dados' => null
            ], 500);
        }

    }

    public function buscarCategoriaProdutoPeloId($id) {
        
        try {

            if (empty($id)) {
                throw new Exception('Informe o id da categoria!');
            }
            
            $id = intval($id);
            $categoria = CategoriaProdutoRepositorio::buscarCategoriaProdutoPeloId($id);

            if (is_null($categoria)) {

                return response()->json([
                    'msg' => 'Categoria não encontrada!',
                    'dados' => null
                ], 404);
            }

            $categoriaDTO = new CategoriaProdutoDTO();
            $categoriaDTO->id = $categoria->id;
            $categoriaDTO->descricao = $categoria->descricao;
            $categoriaDTO->status = $categoria->status === 1 ? true : false;

            return response()->json([
                'msg' => 'Categoria encontrada com sucesso!',
                'dados' => $categoriaDTO
            ], 200);
        } catch (Exception $e) {

            return response()->json([
                'msg' => 'Ocorreu o seguinte erro: ' . $e->getMessage(),
                'dados' => null
            ], 500);
        }

    }

    public function editarCategoriaProduto(Request $req) {
        $req->validate(
            [
                'id' => 'required|numeric',
                'descricao' => 'required',
                'status' => 'required|boolean'
            ],
            [   
                'id.required' => 'Informe o id da categoria!',
                'id.numeric' => 'O id da categoria deve ser um dado numérico inteiro!',
                'descricao.required' => 'Informe a descrição da categoria!',
                'status.boolean' => 'O status da categoria deve ser um dado booleano!',
                'status.required' => 'Informe o status da categoria!'
            ]
        );
        
        try {
            $categoriaProdutoDTO = new CategoriaProdutoDTO();
            $categoriaProdutoDTO->id = intval($req->id);
            $categoriaProdutoDTO->descricao = $req->descricao;
            $categoriaProdutoDTO->status = $req->status;
            $categoria = CategoriaProdutoRepositorio::editarCategoriaProduto($categoriaProdutoDTO);

            if (is_null($categoria)) {

                return response()->json([
                    'msg' => 'Ocorreu um erro ao tentar-se alterar a cetegoria!',
                    'dados' => null
                ], 500);
            }

            return response()->json([
                'msg' => 'Categoria editada com sucesso!',
                'dados' => $categoriaProdutoDTO
            ], 200);
        } catch (NotFoundHttpException $e) {

            return response()->json([
                'msg' => 'Ocorreu o seguinte erro: ' . $e->getMessage(),
                'dados' => null
            ], 404);
        } catch (Exception $e) {
            
            return response()->json([
                'msg' => 'Ocorreu o seguinte erro: ' . $e->getMessage(),
                'dados' => null
            ], 500);
        }

    }

    public function buscarCategoriaProdutoPelaDescricao($descricao) {

        try {
            $categorias = CategoriaProdutoRepositorio::buscarCategoriaProdutoPelaDescricao($descricao);

            if (count($categorias) === 0) {

                return response()->json([
                    'msg' => 'Não existem categorias cadastradas com essa descrição!',
                    'dados' => null
                ], 200);
            }

            $categoriasProdutoDTO = [];

            foreach ($categorias as $categoriaArray) {
                $categoriaDTO = new CategoriaProdutoDTO();
                $categoriaDTO->id = $categoriaArray['id'];
                $categoriaDTO->descricao = $categoriaArray['descricao'];
                $categoriaDTO->status = $categoriaArray['status'] === 1 ? true : false;
                $categoriasProdutoDTO[] = $categoriaDTO;
            }

            return response()->json([
                'msg' => 'Existem categorias cadastradas com essa descrição!',
                'dados' => $categoriasProdutoDTO
            ], 200);
        } catch (Exception $e) {

            return response()->json([
                'msg' => 'Ocorreu o seguinte erro: ' . $e->getMessage(),
                'dados' => null
            ], 500);
        }

    }

    public function buscarCategoriasProdutoAtivas() {

        try {
            $categoriasAtivas = CategoriaProdutoRepositorio::buscarCategoriasProdutoAtivas();

            if (count($categoriasAtivas) === 0) {

                return response()->json([
                    'msg' => 'Não existem categorias ativas!',
                    'dados' => null
                ], 200);
            }

            $categoriasProdutoDTO = [];

            foreach ($categoriasAtivas as $categoriaArray) {
                $categoriaDTO = new CategoriaProdutoDTO();
                $categoriaDTO->id = $categoriaArray['id'];
                $categoriaDTO->descricao = $categoriaArray['descricao'];
                $categoriaDTO->status = $categoriaArray['status'] === 1 ? true : false;
                $categoriasProdutoDTO[] = $categoriaDTO;
            }

            return response()->json([
                'msg' => 'Existem categorias ativas!',
                'dados' => $categoriasProdutoDTO
            ], 200);
        } catch (Exception $e) {

            return response()->json([
                'msg' => 'Ocorreu o seguinte erro: ' . $e->getMessage(),
                'dados' => null
            ], 500);
        }

    }

    public function removerCategoriaProduto($id) {

        try {

            if (empty($id)) {
                throw new Exception('Informe o id da categoria!');
            }

            $produtosRelacionadosACategoriaEmQuestao = ProdutoRepositorio::buscarProdutosPelaCategoria($id);

            if (count($produtosRelacionadosACategoriaEmQuestao) > 0) {

                return response()->json([
                    'msg' => 'Essa categoria está relacionada a produtos, não é possível remover a mesma!',
                    'dados' => null
                ]);
            }

            $categoria = CategoriaProdutoRepositorio::buscarCategoriaProdutoPeloId($id);

            if (empty($categoria)) {

                return response()->json([
                    'msg' => 'Categoria de produto não encontrada!',
                    'dados' => null
                ], 404);
            }

            $res = CategoriaProdutoRepositorio::removerCategoria($id);

            if ($res) {

                return response()->json([
                    'msg' => 'Categoria de produto removida com sucesso!',
                    'dados' => null
                ], 200);
            }

            return response()->json([
                'msg' => 'Ocorreu um erro ao tentar-se remover a categoria em questão!',
                'dados' => null
            ], 500);
        } catch (Exception $e) {

            return response()->json([
                'msg' => 'Ocorreu o seguinte erro: ' . $e->getMessage(),
                'dados' => null
            ], 500);
        }

    }
}