<?php

namespace App\Servico;

use App\Dtos\CategoriaProdutoDTO;
use App\Dtos\ProdutoDTO;
use App\Dtos\UnidadeMedidaDTO;
use App\Repositorio\CategoriaProdutoRepositorio;
use App\Repositorio\ProdutoRepositorio;
use App\Repositorio\UnidadeMedidaRepositorio;
use DateTime;
use DateTimeZone;
use Exception;
use Illuminate\Http\Request;

class ProdutoServico
{

    public function buscarTodosProdutos() {

        try {
            $produtos = ProdutoRepositorio::buscarTodosProdutos();

            if (count($produtos) === 0) {

                return response()->json([
                    'msg' => 'Não existem produtos cadastrados no banco de dados!',
                    'dados' => null
                ], 200);
            }
            
            return response()->json([
                'msg' => 'Produtos encontrados com sucesso!',
                'dados' => $produtos
            ], 200);
        } catch (Exception $e) {

            return response()->json([
                'msg' => 'Ocorreu o seguinte erro: ' . $e->getMessage(),
                'dados' => null
            ], 500);
        }

    }

    public function cadastrarProduto(Request $req) {
        $req->validate([
            'nome' => 'required',
            'descricao' => 'required',
            'precoCompra' => 'required|numeric',
            'precoVenda' => 'required|numeric',
            'status' => 'required|boolean',
            'urlImagemProduto' => 'required',
            'categoriaProdutoId' => 'required|numeric',
            'unidadeMedidaId' => 'required|numeric'
        ],
        [
            'nome.required' => 'Informe o nome do produto!',
            'descricao.required' => 'Informe a descrição do produto!',
            'precoCompra.required' => 'Informe o preço de compra do produto!',
            'precoCompra.numeric' => 'O preço de compra deve ser um valor numérico!',
            'precoVenda.required' => 'Informe o preço de venda do produto!',
            'precoVenda.numeric' => 'O preço de venda deve ser um valor numérico!',
            'status.required' => 'Informe o status do produto!',
            'status.boolean' => 'O status do produto deve ser um dado booleano!',
            'urlImagemProduto.required' => 'Informe a imagem do produto!',
            'categoriaProdutoId.required' => 'Informe a categoria do produto!',
            'categoriaProdutoId.numeric' => 'O id da categoria deve ser um valor numérico!',
            'unidadeMedidaId.required' => 'Informe a unidade de medida do produto!',
            'unidadeMedidaId.numeric' => 'O id da unidade de medida'
        ]);

        try {
            // validando categoria do produto
            $categoria = CategoriaProdutoRepositorio::buscarCategoriaProdutoPeloId($req->categoriaProdutoId);

            if (is_null($categoria)) {

                return response()->json([
                    'msg' => 'Categoria de produto não encontrada!',
                    'dados' => null
                ], 200);
            }

            // validando unidade de medida do produto
            $unidadeMedida = UnidadeMedidaRepositorio::buscarUnidadeMedidaPeloId($req->unidadeMedidaId);

            if (is_null($unidadeMedida)) {

                return response()->json([
                    'msg' => 'Unidade de medida não encontrada!',
                    'dados' => null
                ], 200);
            }

            $produtoDTO = new ProdutoDTO();
            $produtoDTO->nome = $req->nome;
            $produtoDTO->descricao = $req->descricao;
            $produtoDTO->status = $req->status === 1 ? true : false;
            $produtoDTO->precoCompra = $req->precoCompra;
            $produtoDTO->precoVenda = $req->precoVenda;
            $produtoDTO->urlImagemProduto = $req->urlImagemProduto;
            $produtoDTO->categoriaProdutoDTO = new CategoriaProdutoDTO();
            $produtoDTO->categoriaProdutoDTO->id = $req->categoriaProdutoId;
            $produtoDTO->unidadeMedidaDTO = new UnidadeMedidaDTO();
            $produtoDTO->unidadeMedidaDTO->id = $req->unidadeMedidaId;
            $produtoDTO->dataCadastro = new DateTime('now', new DateTimeZone('America/Sao_Paulo'));
            $resultadoCadastrarProduto = ProdutoRepositorio::cadastrarProduto($produtoDTO);

            if (is_null($resultadoCadastrarProduto)) {

                return response()->json([
                    'msg' => 'Ocorreu um erro ao tentar-se cadastrar o produto!',
                    'dados' => null
                ], 500);
            }

            return response()->json([
                'msg' => 'Produto cadastrado com sucesso!',
                'dados' => $resultadoCadastrarProduto
            ], 201);
        } catch (Exception $e) {

            return response()->json([
                'msg' => 'Ocorreu o seguinte erro: ' . $e->getMessage(),
                'dados' => null
            ], 500);
        }

    }

    public function buscarProdutoPeloId($id) {

        try {
            
            if (empty($id)) {
                throw new Exception('Informe o id do produto!');
            }

            $produto = ProdutoRepositorio::buscarProdutoPeloId($id);

            if (empty($produto)) {

                return response()->json([
                    'msg' => 'Não existe um produto cadastrado com esse id!',
                    'dados' => null
                ], 404);
            }

            return response()->json([
                'msg' => 'Produto encontrado com sucesso!',
                'dados' => $produto
            ], 200);
        } catch (Exception $e) {

            return response()->json([
                'msg' => 'Ocorreu o seguinte erro: ' . $e->getMessage(),
                'dados' => null
            ], 500);
        }

    }
}