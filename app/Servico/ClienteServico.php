<?php

namespace App\Servico;

use App\Dtos\ClienteDTO;
use App\Repositorio\ClienteRepositorio;
use DateTime;
use DateTimeZone;
use Exception;
use Illuminate\Http\Request;

class ClienteServico
{

    public function cadastrarCliente(Request $req) {
        $req->validate([
            'nome' => 'required',
            'email' => 'unique:clientes',
            'cpf' => 'required|unique:clientes',
            'rg' => 'required|unique:clientes',
            'telefone' => 'required',
            'status' => 'required|boolean'
        ],
        [   
            'nome.required' => 'Informe o nome do cliente!',
            'email.unique' => 'Já existe um cliente cadastrado com esse e-mail, informe outro e-mail!',
            'cpf.required' => 'Informe o cpf do cliente!',
            'cpf.unique' => 'Já existe um cliente cadastrado com esse cpf, informe outro cpf!',
            'rg.required' => 'Informe o rg do cliente!',
            'rg.unique' => 'Já existe um cliente cadastrado com esse rg, informe outro rg!',
            'telefone.required' => 'Informe o telefone do cliente!',
            'status.required' => 'Informe o status do cliente!',
            'status.boolean' => 'O status do cliente deve ser um dados booleano!'
        ]);
        
        try {
            $clienteDTO = new ClienteDTO();
            $clienteDTO->nome = $req->nome;
            $clienteDTO->email = $req->email;
            $clienteDTO->cpf = $req->cpf;
            $clienteDTO->rg = $req->rg;
            $clienteDTO->telefone = $req->telefone;
            $clienteDTO->sexo = $req->sexo;
            $clienteDTO->status = $req->status;
            // definindo a data de cadastro
            $dateTimeZone = new DateTimeZone('America/Sao_Paulo');
            $dateCadastro = new DateTime('now', $dateTimeZone);
            $clienteDTO->dataCadastro = $dateCadastro->format('Y-m-d H:i:s');
            $clienteCadastrado = ClienteRepositorio::cadastrarCliente($clienteDTO);

            if (empty($clienteCadastrado)) {

                return response()->json([
                    'msg' => 'Ocorreu um erro ao tentar-se cadastrar o cliente!',
                    'dados' => null
                ], 500);
            }

            return response()->json([
                'msg' => 'Cliente cadastrado com sucesso!',
                'dados' => $clienteCadastrado
            ], 201);
        } catch (Exception $e) {

            return response()->json([
                'msg' => 'Ocorreu o seguinte erro: ' . $e->getMessage(),
                'dados' => null
            ], 500);
        }

    }

    public function buscarTodosClientes() {

        try {
            $clientes = ClienteRepositorio::buscarTodosClientes();

            if (count($clientes) === 0) {

                return response()->json([
                    'msg' => 'Não existem clientes cadastrados no banco de dados!',
                    'dados' => null
                ], 200);
            }

            return response()->json([
                'msg' => 'Existem clientes cadastrados no banco de dados!',
                'dados' => $clientes
            ], 200);
        } catch (Exception $e) {

            return response()->json([
                'msg' => 'Ocorreu o seguinte erro: ' . $e->getMessage(),
                'dados' => null
            ], 500);
        }

    }

    public function buscarClientePeloId($id) {

        try {

            if (empty($id)) {

                return response()->json([
                    'msg' => 'Informe o id do cliente!',
                    'dados' => null
                ], 400);
            }
            
            $cliente = ClienteRepositorio::buscarClientePeloId($id);

            if (empty($cliente)) {

                return response()->json([
                    'msg' => 'Cliente não encontrado!',
                    'dados' => null
                ], 404);
            }

            $clienteDTO = new ClienteDTO();
            $clienteDTO->id = $cliente->id;
            $clienteDTO->nome = $cliente->nome;
            $clienteDTO->email = $cliente->email;
            $clienteDTO->rg = $cliente->rg;
            $clienteDTO->cpf = $cliente->cpf;
            $clienteDTO->status = $cliente->status;
            $clienteDTO->sexo = $cliente->sexo;
            $dataCadastro = new DateTime($cliente->dataCadastro);
            $clienteDTO->dataCadastro = $dataCadastro;

            return response()->json([
                'msg' => 'Cliente encontrado com sucesso!',
                'dados' => $clienteDTO
            ], 200);
        } catch (Exception $e) {

            return response()->json([
                'msg' => 'Ocorreu o seguinte erro: ' . $e->getMessage(),
                'dados' => null
            ], 500);
        }

    }
}