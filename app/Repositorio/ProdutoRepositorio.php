<?php

namespace App\Repositorio;

use App\Dtos\CategoriaProdutoDTO;
use App\Dtos\ProdutoDTO;
use App\Dtos\UnidadeMedidaDTO;
use App\Models\Categoria;
use App\Models\Produto;
use App\Models\UnidadeMedida;

class ProdutoRepositorio
{
    
    public static function cadastrarProduto($produtoDTO) {
        $produto = new Produto();
        $produto->nome = $produtoDTO->nome;
        $produto->descricao = $produtoDTO->descricao;
        $produto->precoCompra = $produtoDTO->precoCompra;
        $produto->precoVenda = $produtoDTO->precoVenda;
        $produto->status = $produtoDTO->status;
        $produto->urlImagemProduto = $produtoDTO->urlImagemProduto;
        $produto->dataCadastro = $produtoDTO->dataCadastro->format('Y-m-d H:i:s');
        $produto->categoriaId = $produtoDTO->categoriaProdutoDTO->id;
        $produto->unidadeMedidaId = $produtoDTO->unidadeMedidaDTO->id;

        if ($produto->save()) {
            $produtoDTO->id = $produto->id;

            return $produtoDTO;
        }

        return null;
    }

    public static function buscarTodosProdutos() {
        $produtos = Produto::all();
        $produtosRetorno = [];

        if ($produtos->count() === 0) {

            return [];
        }

        foreach ($produtos->toArray() as $produto) {
            $categoriaId = $produto['categoriaId'];
            $unidadeMedidaId = $produto['unidadeMedidaId'];
            $categoriaProduto = Categoria::find($categoriaId);
            $unidadeMedida = UnidadeMedida::find($unidadeMedidaId);
            $produtosRetorno[] = [
                'id' => $produto['id'],
                'nome' => $produto['nome'],
                'descricao' => $produto['descricao'],
                'precoCompra' => $produto['precoCompra'],
                'precoVenda' => $produto['precoVenda'],
                'status' => $produto['status'],
                'urlImagemProduto' => $produto['urlImagemProduto'],
                'dataCadastro' => $produto['dataCadastro'],
                'categoria' => $categoriaProduto,
                'unidadeMedida' => $unidadeMedida
            ];
        }

        return $produtosRetorno;
    }

    public static function buscarProdutoPeloId($id) {
        $produto = Produto::find($id);

        if (empty($produto)) {

            return null;
        }

        $categoria = Categoria::find($produto->categoriaId);
        $unidadeMedida = UnidadeMedida::find($produto->unidadeMedidaId);
        $produtoDTO = new ProdutoDTO();
        $produtoDTO->id = $produto->id;
        $produtoDTO->nome = $produto->nome;
        $produtoDTO->descricao = $produto->descricao;
        $produtoDTO->precoCompra = $produto->precoCompra;
        $produtoDTO->precoVenda = $produto->precoVenda;
        $produtoDTO->status = $produto->status === 1 ? true : false;
        $produtoDTO->dataCadastro = $produto->dataCadastro;
        $produtoDTO->urlImagemProduto = $produto->urlImagemProduto;
        $categoriaDTO = new CategoriaProdutoDTO();
        $categoriaDTO->id = $categoria->id;
        $categoriaDTO->descricao = $categoria->descricao;
        $categoriaDTO->status = $categoria->status === 1 ? true : false;
        $unidadeMedidaDTO = new UnidadeMedidaDTO();
        $unidadeMedidaDTO->id = $unidadeMedida->id;
        $unidadeMedidaDTO->descricao = $unidadeMedida->descricao;
        $unidadeMedidaDTO->status = $unidadeMedida->status === 1 ? true : false;
        $produtoDTO->categoriaProdutoDTO = $categoriaDTO;
        $produtoDTO->unidadeMedidaDTO = $unidadeMedidaDTO;

        return $produtoDTO;
    }

    public static function editarProduto($produtoDTO) {

    }

    public static function buscarProdutoPelaDescricao($descricao) {

    }

    public static function buscarProdutosPelaCategoria($idCategoria) {
        $produtos = Produto::where('categoriaId', $idCategoria)->get();
        
        if ($produtos->count() === 0) {

            return [];
        }   

        $produtos = $produtos->toArray();
        $produtosRetorno = [];

        foreach ($produtos as $produto) {
            $categoriaId = $produto['categoriaId'];
            $unidadeMedidaId = $produto['unidadeMedidaId'];
            $categoriaProduto = Categoria::find($categoriaId);
            $unidadeMedida = UnidadeMedida::find($unidadeMedidaId);
            $produtosRetorno[] = [
                'id' => $produto['id'],
                'nome' => $produto['nome'],
                'descricao' => $produto['descricao'],
                'precoCompra' => $produto['precoCompra'],
                'precoVenda' => $produto['precoVenda'],
                'status' => $produto['status'],
                'urlImagemProduto' => $produto['urlImagemProduto'],
                'dataCadastro' => $produto['dataCadastro'],
                'categoria' => $categoriaProduto,
                'unidadeMedida' => $unidadeMedida
            ];
        }

        return $produtosRetorno;
    }
}