<?php

namespace App\Repositorio;

use App\Models\Cliente;

class ClienteRepositorio
{

    public static function cadastrarCliente($clienteDTO) {
        $cliente = new Cliente();
        $cliente->nome = $clienteDTO->nome;
        $cliente->cpf = $clienteDTO->cpf;
        $cliente->rg = $clienteDTO->rg;
        $cliente->email = $clienteDTO->email;
        $cliente->telefone = $clienteDTO->telefone;
        $cliente->dataCadastro = $clienteDTO->dataCadastro;
        $cliente->status = $clienteDTO->status;
        $cliente->sexo = $clienteDTO->sexo;

        if ($cliente->save()) {

            $clienteDTO->id = $cliente->id;

            return $clienteDTO;
        }

        return null;
    }

    public static function buscarTodosClientes() {

        return Cliente::all()->toArray();
    }

    public static function buscarClientePeloId($id) {
        
        return Cliente::find($id);
    }
}