<?php

namespace App\Repositorio;

use App\Models\Categoria;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CategoriaProdutoRepositorio
{
    
    public static function cadastrarCategoriaProduto($categoriaProdutoDTO) {
        $categoria = new Categoria();
        $categoria->descricao = $categoriaProdutoDTO->descricao;
        $categoria->status = $categoriaProdutoDTO->status;
        
        if ($categoria->save()) {
            $categoriaProdutoDTO->id = $categoria->id;

            return $categoriaProdutoDTO;
        }

        return null;
    }

    public static function buscarTodasCategoriasProduto() {

        return Categoria::all()->toArray();
    }

    public static function buscarCategoriaProdutoPeloId($id) {

        return Categoria::find($id);
    }

    public static function buscarCategoriasProdutoAtivas() {

        return Categoria::where('status', true)->get()->toArray();
    }

    public static function buscarCategoriaProdutoPelaDescricao($descricao) {

        return Categoria::where('descricao', 'LIKE', '%' . $descricao . '%')->get()->toArray();
    }

    public static function editarCategoriaProduto($categoriaProdutoDTO) {
        $categoriaEditar = Categoria::find($categoriaProdutoDTO->id);
        
        if (is_null($categoriaEditar)) {
            throw new NotFoundHttpException('Categoria de produto não encontrada!');
        }
        
        $resEditarCategoriaProduto = $categoriaEditar->update([
            'descricao' => $categoriaProdutoDTO->descricao,
            'status' => $categoriaProdutoDTO->status
        ]);

        if ($resEditarCategoriaProduto) {
            return $categoriaProdutoDTO;
        }

        return null;
    }

    public static function removerCategoria($id) {

        return Categoria::where('id', $id)->delete();
    }
}