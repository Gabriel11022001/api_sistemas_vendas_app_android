<?php

namespace App\Repositorio;

use App\Models\UnidadeMedida;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UnidadeMedidaRepositorio
{

    public static function cadastrarUnidadeMedida($unidadeMedidaDTO) {
        $unidadeMedida = new UnidadeMedida();
        $unidadeMedida->descricao = $unidadeMedidaDTO->descricao;
        $unidadeMedida->status = $unidadeMedidaDTO->status;

        if ($unidadeMedida->save()) {
            $unidadeMedidaDTO->id = $unidadeMedida->id;

            return $unidadeMedidaDTO;
        }

        return null;
    }

    public static function buscarTodasUnidadesMedida() {

        return UnidadeMedida::all()->toArray();
    }

    public static function buscarUnidadeMedidaPeloId($id) {

        return UnidadeMedida::find($id);
    }

    public static function editarUnidadeMedida($unidadeMedidaDTO) {
        $unidadeMedidaEditar = UnidadeMedida::find($unidadeMedidaDTO->id);

        if (is_null($unidadeMedidaEditar)) {
            throw new NotFoundHttpException('Unidade de medida não encontrada!');
        }

        $resEditarUnidadeMedida = $unidadeMedidaEditar->update([
            'descricao' => $unidadeMedidaDTO->descricao,
            'status' => $unidadeMedidaDTO->status
        ]);

        if ($resEditarUnidadeMedida) {

            return $unidadeMedidaDTO;
        }

        return null;
    }

    public static function alterarStatusUnidadeMedida($id, $status) {
        $unidadeMedida = UnidadeMedida::find($id);

        if (is_null($unidadeMedida)) {
            throw new NotFoundHttpException('Unidade de medida não encontrada!');
        }

        $resultadoEditarStatusUnidadeMedida = $unidadeMedida->update([
            'status' => $status
        ]);

        if ($resultadoEditarStatusUnidadeMedida) {

            return $unidadeMedida;
        }

        return null;
    }

    public static function buscarUnidadeMedidaPelaDescricao($descricao) {
        $unidadeMedida = UnidadeMedida::where('descricao', $descricao)->get()->toArray();
        
        return $unidadeMedida;
    }
}