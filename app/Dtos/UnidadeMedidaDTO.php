<?php

namespace App\Dtos;

class UnidadeMedidaDTO
{
    public $id;
    public $descricao;
    public $status;

    public function __construct() {
        $this->status = true;
    }
}