<?php

namespace App\Dtos;

class CategoriaProdutoDTO
{
    public $id;
    public $descricao;
    public $status;

    public function __construct() {
        $this->id = 0;
    }
}