<?php

namespace App\Dtos;

class ProdutoDTO
{
    public $id;
    public $nome;
    public $descricao;
    public $precoCompra;
    public $precoVenda;
    public $status;
    public $dataCadastro;
    public $urlImagemProduto;
    public $categoriaProdutoDTO;
    public $unidadeMedidaDTO;

    public function __construct() {
        $this->status = true;
    }
}