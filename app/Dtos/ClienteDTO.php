<?php

namespace App\Dtos;

class ClienteDTO
{
    public $id;
    public $nome;
    public $cpf;
    public $rg;
    public $email;
    public $telefone;
    public $dataCadastro;
    public $sexo;
    public $status;

    public function __construct() {
        $this->status = true;
    }
}