<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $fillable = ['id', 'nome', 'descricao', 'precoCompra', 'precoVenda', 'status', 'dataCadastro', 'categoriaId', 'unidadeMedidaId', 'urlImagemProduto'];

    public function categoria() {

        return $this->hasOne(Categoria::class);
    }

    public function unidadeMedida() {

        return $this->hasOne(UnidadeMedida::class);
    }
}
