<?php

namespace App\Http\Controllers;

use App\Models\UnidadeMedida;
use App\Servico\UnidadeMedidaServico;
use Illuminate\Http\Request;

class UnidadeMedidaController extends Controller
{
    private $unidadeMedidaServico;

    public function __construct(UnidadeMedidaServico $unidadeMedidaServico) {
        $this->unidadeMedidaServico = $unidadeMedidaServico;
    }

    public function cadastrarUnidadeMedida(Request $req) {

        return $this->unidadeMedidaServico->cadastrarUnidadeMedida($req);
    }

    public function buscarTodasUnidadesMedida() {

        return $this->unidadeMedidaServico->buscarTodasUnidadesMedida();
    }

    public function buscarUnidadeMedidaPeloId($id) {

        return $this->unidadeMedidaServico->buscarUnidadeMedidaPeloId($id);
    }

    public function alterarStatusUnidadeMedida(Request $req) {

        return $this->unidadeMedidaServico->alterarStatusUnidadeMedida($req);
    }

    public function editarUnidadeMedida(Request $req) {

        return $this->unidadeMedidaServico->editarUnidadeMedida($req);
    }
}
