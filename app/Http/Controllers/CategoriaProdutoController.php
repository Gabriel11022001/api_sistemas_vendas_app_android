<?php

namespace App\Http\Controllers;

use App\Servico\CategoriaProdutoServico;
use Illuminate\Http\Request;

class CategoriaProdutoController extends Controller
{
    private $categoriaProdutoServico;

    public function __construct(CategoriaProdutoServico $categoriaProdutoServico) {
        $this->categoriaProdutoServico = $categoriaProdutoServico;
    }

    public function cadastrarCategoriaProduto(Request $req) {

        return $this->categoriaProdutoServico->cadastrarCategoriaProduto($req);
    }

    public function buscarTodasCategoriasProduto() {

        return $this->categoriaProdutoServico->buscarTodasCategoriasProduto();
    }

    public function buscarCategoriaProdutoPeloId($id) {

        return $this->categoriaProdutoServico->buscarCategoriaProdutoPeloId($id);
    }

    public function buscarCategoriasProdutoAtivas() {

        return $this->categoriaProdutoServico->buscarCategoriasProdutoAtivas();
    }

    public function buscarCategoriaProdutoPelaDescricao($descricao) {

        return $this->categoriaProdutoServico->buscarCategoriaProdutoPelaDescricao($descricao);
    }

    public function editarCategoriaProduto(Request $req) {

        return $this->categoriaProdutoServico->editarCategoriaProduto($req);
    }

    public function removerCategoria($id) {

        return $this->categoriaProdutoServico->removerCategoriaProduto($id);
    }
}
