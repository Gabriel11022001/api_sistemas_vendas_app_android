<?php

namespace App\Http\Controllers;

use App\Servico\ClienteServico;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    private $clienteServico;

    public function __construct(ClienteServico $clienteServico) {
        $this->clienteServico = $clienteServico;
    }

    public function cadastrarCliente(Request $req) {

        return $this->clienteServico->cadastrarCliente($req);
    }

    public function buscarTodosClientes() {

        return $this->clienteServico->buscarTodosClientes();
    }

    public function buscarClientePeloId($id) {

        return $this->clienteServico->buscarClientePeloId($id);
    }
}
