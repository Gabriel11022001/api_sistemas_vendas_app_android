<?php

namespace App\Http\Controllers;

use App\Servico\ProdutoServico;
use Illuminate\Http\Request;

class ProdutoController extends Controller
{
    private $produtoServico;

    public function __construct(ProdutoServico $produtoServico) {
        $this->produtoServico = $produtoServico;
    }

    public function buscarTodosProdutos() {
        
        return $this->produtoServico->buscarTodosProdutos();
    }

    public function cadastrarProduto(Request $req) {

        return $this->produtoServico->cadastrarProduto($req);
    }

    public function buscarProdutoPeloId($id) {

        return $this->produtoServico->buscarProdutoPeloId($id);
    }
}
