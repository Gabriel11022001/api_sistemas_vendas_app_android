<?php

use App\Http\Controllers\CategoriaProdutoController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\ProdutoController;
use App\Http\Controllers\UnidadeMedidaController;
use Illuminate\Support\Facades\Route;

Route::post('/categoria-produto', [CategoriaProdutoController::class, 'cadastrarCategoriaProduto']);
Route::post('/unidade-medida', [UnidadeMedidaController::class, 'cadastrarUnidadeMedida']);
Route::post('/produto', [ProdutoController::class, 'cadastrarProduto']);
Route::post('/cliente', [ClienteController::class, 'cadastrarCliente']);
Route::put('/categoria-produto', [CategoriaProdutoController::class, 'editarCategoriaProduto']);
Route::put('/unidade-medida', [UnidadeMedidaController::class, 'alterarStatusUnidadeMedida']);
Route::put('/unidade-medida', [UnidadeMedidaController::class, 'editarUnidadeMedida']);
Route::delete('/categoria-produto/{id}', [CategoriaProdutoController::class, 'removerCategoria']);
Route::get('/categoria-produto', [CategoriaProdutoController::class, 'buscarTodasCategoriasProduto']);
Route::get('/categoria-produto/buscar-categorias-ativas', [CategoriaProdutoController::class, 'buscarCategoriasProdutoAtivas']);
Route::get('/unidade-medida', [UnidadeMedidaController::class, 'buscarTodasUnidadesMedida']);
Route::get('/produto', [ProdutoController::class, 'buscarTodosProdutos']);
Route::get('/cliente', [ClienteController::class, 'buscarTodosClientes']);
Route::get('/categoria-produto/{id}', [CategoriaProdutoController::class, 'buscarCategoriaProdutoPeloId']);
Route::get('/categoria-produto/buscar-pela-descricao/{descricao}', [CategoriaProdutoController::class, 'buscarCategoriaProdutoPelaDescricao']);
Route::get('/unidade-medida/{id}', [UnidadeMedidaController::class, 'buscarUnidadeMedidaPeloId']);
Route::get('/produto/{id}', [ProdutoController::class, 'buscarProdutoPeloId']);
Route::get('/cliente/{id}', [ClienteController::class, 'buscarClientePeloId']);
